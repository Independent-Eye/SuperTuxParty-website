# Source Code

## Super Tux Party
The code repository is hosted on [GitLab.com](https://gitlab.com/supertuxparty/supertuxparty)  
See the [README.md](https://gitlab.com/SuperTuxParty/SuperTuxParty/-/blob/dev/README.md) file for further instructions

## Super Tux Party Website
The code for this website is hosted on [GitLab.com](https://gitlab.com/SuperTuxParty/SuperTuxParty-website)

### Third party material we use on this website:
* [Font Awesome](https://fontawesome.com) svg icons. Licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

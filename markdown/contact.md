# Contact

The best way of contacting us is through one of these channels:

## GitLab
Whenever you've found a bug or got an idea for improvement, please add them to our [issue tracker](https://gitlab.com/supertuxparty/supertuxparty/issues).

## Matrix
Most communication happens on [Matrix](https://matrix.org), a free messaging protocol.

Join the channel [#SuperTuxParty-Extra:matrix.org](https://matrix.to/#/#SuperTuxParty-Extra:matrix.org) for general discussion of the project.

We also have [#SuperTuxParty-Dev:matrix.org](https://matrix.to/#/#SuperTuxParty-Dev:matrix.org) for discussion regarding development.

## Reddit
If you're more familiar with Reddit than Matrix, you can create a post on our [subreddit](https://reddit.com/r/supertuxparty)

## Weblate
Help translate Super Tux Party at [Hosted Weblate](https://hosted.weblate.org/projects/super-tux-party/). Add your own language if it is missing.

---
# Responsible for content on this site
**Name**: Florian Kothmeier

<p style="display: inline;">
	<strong>Address</strong>:
	<div style="display: inline-block; vertical-align: top; margin: 0;">
		Fahrstraße 6,<br>
		91054 Erlangen, Germany
	</div>
</p>

**E-mail**: [info@supertux.party](mailto:info@supertux.party)

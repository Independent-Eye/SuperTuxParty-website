from app import app, API_TOKEN
from flask import render_template, send_from_directory, abort, request, redirect

from pathlib import Path

import os

class Post:
    def __init__(self, target, header, content):
        self.target = target
        self.header = header
        self.content = content

def sanitize_path(root, path):
    try:
        full_path = Path(root).joinpath(path).resolve()
        full_path.relative_to(Path(root).resolve())
        return full_path
    except:
        abort(400)

def render_markdown(source_file, css_class="", stylesheets=[]):
    with open(source_file, 'r') as f:
        return render_template('markdown.html', content=f.read(), css_class=css_class, stylesheets=stylesheets)

def read_post(path):
    with open(path, 'r') as f:
        content = f.read()
        lines = content.split('\n')
        return Post(path[:-3], lines[0], '\n'.join(lines[1:]))

def get_blog_entries(offset = None, limit = None):
    files = ['blog/' + x for x in os.listdir('blog') if not x.endswith('.hidden')]
    files.sort(key=os.path.getmtime, reverse=True) # Only return the newest elements

    if offset and limit:
        files = files[offset:offset+limit]
    elif offset:
        files = files[offset:]
        
    return [read_post(f) for f in files]

@app.route('/')
def index():
    return render_template('index.html', posts=get_blog_entries(offset=0, limit=10)) # Display the ten most recent entries

@app.route('/blog')
def blog():
    return render_template('index.html', posts=get_blog_entries())

@app.route('/blog/<name>')
def display_blogpost(name):
    try:
        return render_markdown(sanitize_path('blog/', name + '.md'), css_class="post", stylesheets=["/css/post.css"])
    except FileNotFoundError:
        abort(404)

@app.route('/download')
def download_redirect():
    return redirect('/download/latest')

@app.route('/download/<name>')
def download(name):
    path = sanitize_path('downloads/', name)

    if not path.exists():
        abort(404)
    
    return render_template('download.html', release=name, version=path.name)

@app.route('/download/<name>/<os>')
def download_file(name, os):
    return send_from_directory('downloads', name + "/" + os + ".zip", as_attachment=True, attachment_filename="super-tux-party-{}-{}.zip".format(os, name))

@app.route('/upload/<name>/<os>', methods=['POST'])
def upload(name, os):
    api_token = request.headers.get('X-API-Token')
    if api_token != API_TOKEN:
        abort(403)

    if not 'data' in request.files:
        abort(400)
    data = request.files['data']

    if name == 'latest':
        abort(400)

    path = sanitize_path('downloads', name)

    if not path.exists():
        path.mkdir()

    path = sanitize_path(path, os + '.zip')

    data.save(path)
    
    return '', 201

@app.route('/source')
def source():
    return render_markdown("markdown/source.md")

@app.route('/contact')
def contact():
    return render_markdown("markdown/contact.md")
    
